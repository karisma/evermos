<?php

//soal no 1
class Tennis
{
    //ball container unlimited , jadi saya pakai array untuk handle situasi ini
    private $ballContainer = [];
    //jumlah bola per container untuk threshold mark as verifiednya
    private $maximumBallEachContainer;

    public function __construct($maximumBallEachContainer = 3)
    {
        //saya baca-baca maximum bola per contianer untuk tennis adalah 3, namun ini bisa dioverride sewaktu2
        $this->maximumBallEachContainer = $maximumBallEachContainer;
    }

    //ini untuk menambahkan container bolanya
    public function addBallContainer()
    {
        array_push($this->ballContainer, 0);
        echo "new Ball Container \n";
    }

    //fungsi untuk melihat kontainer bola saat ini
    public function getBallContainer()
    {
        return $this->ballContainer;
    }

    //di soal disebutkan bahwa secara acak si pemain akan memasukkan bola
    public function addBallToRandomContainer()
    {
        //dicheck terlebih dahulu apakah sudah ada container yang tersedia
        if ($this->checkAvailableContainer()) {
            //ambil secara acak containernya
            $random = array_rand($this->ballContainer);
            $this->ballContainer[$random]++;
            echo "Added Ball To Container: " . ($random + 1) . "\n";
        } else {
            echo "Add Ball To Container failed because no Ball container provided\n";
        }
    }

    //melihat apakah sudah ada container atau belum
    public function checkAvailableContainer()
    {
        return count($this->ballContainer) > 0;
    }

    //fungsi untuk mengambil bola dari kontainer secara acak
    public function removeBallFromRandomContainer()
    {
        //dicheck terlebih dahulu apakah sudah ada container yang tersedia
        if ($this->checkAvailableContainer()) {
            // ambil secara acak containernya
            $random = array_rand($this->ballContainer);
            //check apakah di dalam kontainer tersebut ada isinya
            if ($this->ballContainer[$random] !== 0) {
                //kalau ada, maka ambil bola dari kontainer tersebut
                $this->ballContainer[$random]--;
                echo "Removed Ball From Container: " . ($random + 1) . "\n";
            } else {
                //nyatakan gagal dikarenakan kontainer sudah kosong
                echo "Remove Ball From Container: " . ($random + 1) . " failed because its already empty\n";
            }
        } else {
            //container belum ada
            echo "Remove Ball From Container failed because no Ball container provided\n";
        }
    }

    //fungsi untuk mengecek apakah sudah ada kontainer yang mark as verified (slot penuh)
    public function checkVerified()
    {
        //di filter berdasarkan value dari container tersebut apakah sudah mencapai threshold nya
        $a = array_filter($this->ballContainer, function ($array) {
            return $array === $this->maximumBallEachContainer;
        });
        //jika ya
        if (count($a) > 0) {
            return true;
        }
        //jika tidak
        return false;
    }

    //fungsi untuk menentukan index array manakan yang sudah penuh
    public function getNumberOfVerifiedBallContainer()
    {
        $a = array_filter($this->ballContainer, function ($array) {
            return $array === $this->maximumBallEachContainer;
        });
        if (count($a) > 0) {
            return array_keys($a)[0];
        }
        return null;
    }
}

//soal no 2, kalau untuk API sebenernya bisa saja, kalau main database / cookie / cache(redis) karena saya menggunakan hanya menggunakan
// variable di dalam class Tennis tersebut, cmn diatas saya sudah buatkan fungsi2 yang
//sifatnya SOLID Principle, apabila dibuatkan RESTFULL API maka tinggal buat routing dan di alihkan ke fungsi2 tersebut
//namun dibawah ini saya buatkan simulasinya
$tennis = new Tennis();
$countAutomatic = 1;
//jika belum ada yang verified
while (!$tennis->checkVerified()) {
    $countAutomatic++;
    $action = rand(1, 3);
    //saya buatkan random angka dari 1 sampai 3
    switch ($action) {
        case 1:
            //jika keluar angka 1 maka akan menambah container
            $tennis->addBallContainer();
            break;
        case 2:
            //jika keluar angka 2 maka akan menambahkan bola ke container secara acak
            $tennis->addBallToRandomContainer();
            break;
        case 3:
            //jika keluar angka 3 maka akan mengeluarkan bola dari kontainer secara acak
            $tennis->removeBallFromRandomContainer();
            break;
        default:
            echo "Invalid arguments";
    }
}
print_r($tennis->getBallContainer());
echo "Berhasil simulasi dengan total percobaan " . $countAutomatic . "\n";
echo "Posisi Ball Container yang penuh (verified) terdapat pada Index " . $tennis->getNumberOfVerifiedBallContainer() . " pada array, dan urutan " . ($tennis->getNumberOfVerifiedBallContainer() + 1) . " pada realnya\n";
echo "\n\n\n\n\n";


//Soal no 3

class Treasure
{
    //inisialisasi variable untuk board, dan posisi awal
    protected $board = [];
    protected $start = [];

    public function __construct($width = 8, $height = 6)
    {
        //input data awal
        for ($i = 0; $i < $width; $i++) {
            for ($j = 0; $j < $height; $j++) {
                $this->board[$j][$i] = 1;
            }
        }
        $this->start = [4, 1];
    }

    //mendapatkan posisi awal
    public function getStartPosition()
    {
        return $this->start;
    }

    //mendapatkan board
    public function getBoard()
    {
        return $this->board;
    }

    // visualisasi board nya
    public function showBoard()
    {
        foreach ($this->board as $x => $width) {
            foreach ($width as $y => $value) {
                if ($x === $this->start[0] && $y === $this->start[1]) {
                    echo "X ";
                } else {
                    echo $value === 0 ? "# " : ". ";
                }

            }
            echo "\n";
        }
    }

    // penambahka rintangan
    public function addObstacle($posX, $posY)
    {
        $this->board[$posY][$posX] = 0;
    }
}

//hampir sama dengan soal no 1, kalao kita ada penyimpanan baik di cache , atau, di cookie atau di database, untuk penggunaan API mudah
// dibawah ini saya buatkan simulasinya
$treasure = new Treasure();
$treasure->addObstacle(0, 0);
$treasure->addObstacle(1, 0);
$treasure->addObstacle(2, 0);
$treasure->addObstacle(3, 0);
$treasure->addObstacle(4, 0);
$treasure->addObstacle(5, 0);
$treasure->addObstacle(6, 0);
$treasure->addObstacle(7, 0);

$treasure->addObstacle(0, 1);
$treasure->addObstacle(7, 1);

$treasure->addObstacle(0, 2);
$treasure->addObstacle(2, 2);
$treasure->addObstacle(3, 2);
$treasure->addObstacle(4, 2);
$treasure->addObstacle(7, 2);

$treasure->addObstacle(0, 3);
$treasure->addObstacle(4, 3);
$treasure->addObstacle(6, 3);
$treasure->addObstacle(7, 3);

$treasure->addObstacle(0, 4);
$treasure->addObstacle(2, 4);
$treasure->addObstacle(7, 4);

$treasure->addObstacle(0, 5);
$treasure->addObstacle(1, 5);
$treasure->addObstacle(2, 5);
$treasure->addObstacle(3, 5);
$treasure->addObstacle(4, 5);
$treasure->addObstacle(5, 5);
$treasure->addObstacle(6, 5);
$treasure->addObstacle(7, 5);

$treasure->showBoard();
//inisialisasi data untuk looping kedepannya
$startX = $treasure->getStartPosition()[1];
$startY = $treasure->getStartPosition()[0];
$countY = 0;
$northStep = "";
//langkah ke atas akan terus di ulang sampai 0 karena array dari atas kebawah sedangkan posisi ada di 3
while ($startY >= 0) {
    $northStep .= $startX . "-" . $startY . " ";
    //inisialisasi untuk pergi ke arah kanan
    $startX2 = $startX + 1;
    // hitung langkah otomatis
    $countY++;
    $countX = 0;
    $eastStep = "";
    //di check apakah ada jalan ke arah kanan yang tidak terhalang obstacle
    while ($treasure->getBoard()[$startY][$startX2] === 1) {
        //apabila tidak terhalang inisialisasi untuk perulangan jalan ke arah bawah
        $countX++;
        $eastStep .= $startX2 . "-" . $startY . " ";
        $startY2 = $startY + 1;
        $countY2 = 0;
        //dicheck apakah untuk setiap kali dy jalan ke kaan, apakah ada jalan ke bawah untuk dilalui tanpa penghalang
        while ($treasure->getBoard()[$startY2][$startX2] === 1) {
            $countY2++;
            echo "Available path(x,y) : " . $northStep . $eastStep . " " . $startX2 . "-" . $startY2;
            $startY2++;
            echo "\n Total dot count: " . ($countY + $countX + $countY2) . "\n";
        }
        $startX2++;
    }
    $startY--;
}
echo "Apabila menghendaki total semua titik maka dipilihlah variable dot count yang paling besar\n";
echo "Apabila menghendaki kemungkindan route terpendek maka dipilihlah variable dot count yang paling kecil\n";

//Soal no 2
//Untuk mengatasi situasi limited stock namun dengan request yang datang banyak secara bersamaan
//Maka dibutuhkan penampungan sementara (bisa menggunakan redis) , agar bisa dilakukan queue/antrian dalam interval tertentu untuk mengatasi
//nilai stock yang bisa minus dikarenakan request yangd atang bersamaan

class Order
{
    //disini saya simulasikan $requestOrder adalah penyimpanan sementara di queue
    private $autoIncrement = 0;
    private $product = [];
    private $order = [];
    private $requestOrder = [];

    public function __construct()
    {

        try {
            $this->product = [
                [
                    'id' => 1,
                    'stock' => 2
                ],
                [
                    'id' => 2,
                    'stock' => 4
                ]
            ];
        } catch (Exception $exception) {
            print_r($exception);
        }
    }

    //memasukan request order kedalam queue
    public function addTempOrder($id, $quantity = 1)
    {
        if ($this->checkProduct($id)) {
            $this->requestOrder[] = [
                'id' => ++$this->autoIncrement,
                'product_id' => $id,
                'quantity' => $quantity
            ];
        }


    }

    //check ketersediaan product
    public function checkProduct($id)
    {
        $check = $this->getDetailProduct($id);
        if (count($check) > 0 && $check[array_keys($check)[0]]['stock'] > 0) {
            return true;
        }

        return false;
    }

    // lihat detail product
    public function getDetailProduct($id)
    {
        $check = array_filter($this->product, function ($product) use ($id) {
            return $product['id'] === $id;
        });
        return $check;
    }

    //proses queue * kalao di laravel pakai Job Queue
    public function queueOrderJob()
    {
        foreach ($this->requestOrder as $key => $requestOrder) {
            $product = $this->getDetailProduct($requestOrder['product_id']);
            if ($requestOrder['quantity'] <= $product[array_keys($product)[0]]['stock']) {
                $this->order[] = $requestOrder;
                $this->product[array_keys($product)[0]]['stock'] -= $requestOrder['quantity'];
            }
            unset($this->requestOrder[$key]);
        }
    }

    //Melihat Request Data
    public function getRequestOrderData()
    {
        return $this->requestOrder;
    }

    //melihat hasil akhir order yang dibuat
    public function getOrderData()
    {
        return $this->order;
    }
}

$order = new Order();

$order->addTempOrder(1);
$order->addTempOrder(1);
$order->addTempOrder(2);
$order->addTempOrder(1);
$order->queueOrderJob();
print_r($order->getOrderData());
